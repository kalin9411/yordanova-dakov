const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cors = require('cors')

const app = express()
// app.use(bodyParser)
app.use(bodyParser.json())
app.use(cors({
    origin: 'https://yana-filip-client.herokuapp.com'
  }))


mongoose.connect('mongodb+srv://kalin116:yanafilip@cluster0.97oou.mongodb.net/yordanova-dakov?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true},async (err, response) => {
    if (err) {
        throw new Error(err)
    } else {
        console.log('connected to db')
    }
})

app.post('/question', async (req, res) => {
    try {
    const questions =  mongoose.connection.collection('questions')
    await questions.insertOne(req.body)
    return res.status(200).send()
    } catch(error) {
        return res.send(402).send(error)
    }
})

app.get('/question', async (req, res) => {
    let result
    try {
    const questions =  mongoose.connection.collection('questions')
    questions.find({}).toArray((err, data) => {
        // console.log(data)
        result = data
        return res.send(result)
    })
    } catch (error) {
        return res.status(402).send(error)
    }
})

app.get('/data', async (req, res) => {
    let result
    try {
    const invitations =  mongoose.connection.collection('invitations')
    invitations.find({}).toArray((err, data) => {
        // console.log(data)
        result = data
        return res.send(result)
    })
    } catch (error) {
        return res.status(402).send(error)
    }
})


app.get('/log', async (req, res) => {
    let result
    try {
    const log =  mongoose.connection.collection('log')
    log.find({}).toArray((err, data) => {
        // console.log(data)
        result = data
        return res.send(result)
    })
} catch (error) {
        console.log(error)
        return res.status(402).send(error)
    }

})

app.post('/invitation/:id', async (req, res) => {
    let result
    try {
    const invitations =  mongoose.connection.collection('invitations')
     result = await invitations.insertOne(req.body)
    } catch (error) {
        return res.status(402).send(error)
    }
    return res.send(result)
})

app.get('/invitation/:id/seen', async (req, res) => {
    let result
    try {
    const invitations =  mongoose.connection.collection('invitations')
     await invitations.updateOne({number: +req.params.id}, {$set: {seen: true}})
     result = await invitations.findOne({number: +req.params.id})
    const log =  mongoose.connection.collection('log')
    log.insertOne({result, date: new Date(), message: 'seen'})
    } catch (error) {
        return res.status(402).send(error)
    }
    return res.send(result)
})
app.put('/invitation/:id', async (req, res) => {
    let result
    try {
    const invitations =  mongoose.connection.collection('invitations')
     await invitations.updateOne({number: +req.params.id}, {'$set': req.body})
     result = await invitations.findOne({number: +req.params.id})
     const log =  mongoose.connection.collection('log')
     log.insertOne({result, date: new Date(), message: 'updated'})
    } catch (error) {
        console.log(error);
        return res.status(404).send(error)
    }

    return res.send(result)
})

app.get('/invitation/:number', async (req, res) => {
    let result
    try {
    const invitations =  mongoose.connection.collection('invitations')
    result = await invitations.findOne({number: +req.params.number})
    // console.log(req.params.number, result)
     return res.send(result)
    } catch (error) {
        return res.status(402).send(error)
    }
})

app.listen(process.env.PORT, () => {
    // connection.
    console.log('server running on port 4000')
})  
