import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from './../environments/environment';
import { Invitation } from './models/Models';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  invitation: BehaviorSubject<Invitation> = new BehaviorSubject(
    {
      email: '',
      phone: '',
      numberOfGuests: 0,
      invited: [],
      additionalGuests: [],
      seen: false,
      answered: false,
      kids: 0,
      plusOne: 0,
      attend: 1
    });


  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute
    ) {
   }

  getLog() {
    return this.http.get(`${environment.apiUrl}/log`)
  }

  getQuestions() {
    return this.http.get(`${environment.apiUrl}/question`)
  }

  getData() {
    return this.http.get(`${environment.apiUrl}/data`)
  }

  getInvitation(id) {
    if (!id) {
      id = localStorage.getItem('id');
    }
    if (!id) {
      this.router.navigate(['not-found']);
    } else {
      this.router.navigate([],
        {
          relativeTo: this.route,
          queryParams: {invitation: id},
        });
      this.http.get(`${environment.apiUrl}/invitation/${id}`).subscribe((data: Invitation) => {
        this.invitation.next(data as any);
        if (!data.additionalGuests) {
          data.additionalGuests = []
        }
        localStorage.setItem('id', `${data.number}`)
        this.seenInvitation(id);
      });
    }
  }

  seenInvitation(id) {
    this.http.get(`${environment.apiUrl}/invitation/${id}/seen`).subscribe(data => {
    })
  }

  updateInvitation(id, object: Invitation) {
    return this.http.put(`${environment.apiUrl}/invitation/${id}`, object);
  }

  sendQuestion(question: {email: string, question: string}) {
    return this.http.post(`${environment.apiUrl}/question`, question);
  }
}
