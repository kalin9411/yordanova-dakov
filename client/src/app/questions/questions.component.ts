import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { Invitation } from '../models/Models';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  invitation: Invitation;

  isUpdated

  isLoading

  question = {email: '', question: ''};

  constructor(private backendService: BackendService) { }

  ngOnInit(): void {
    this.backendService.invitation.subscribe(data => {
      this.invitation = data;
      this.question.email = this.invitation.email;
    });
  }

  isEmailValid(): boolean | string {
    if (!this.invitation.email) {
      return '';
    }
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.invitation.email).toLowerCase());
  }

  sendQuestion() {
    this.isLoading = true
    this.backendService.sendQuestion(this.question).subscribe(data => {
      console.log(data);
      this.isUpdated = true
      this.isLoading = false;
    });
  }
}
