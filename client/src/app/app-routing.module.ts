import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { MainComponent } from './main/main.component';
import { NotfoundComponent } from './notfound/notfound.component';


const routes: Routes = [
  {path: 'invitation', component: MainComponent},
  {path: 'sandpiper-log', component: AdminComponent},
  {path: 'not-found', component: NotfoundComponent},
  {path: '', redirectTo: 'invitation', pathMatch: 'full'},
{ path: '**', redirectTo: 'not-found', pathMatch: 'full'}
] // Wildcard route for a 404 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
