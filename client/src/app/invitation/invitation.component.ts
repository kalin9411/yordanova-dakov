import { Component, OnInit, Input } from '@angular/core';
import { BackendService } from '../backend.service';
import { Invitation } from '../models/Models';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {
  @Input()
  invitation: Invitation;

  constructor(private backendService: BackendService) {
  }
  ngOnInit(): void {
    this.backendService.invitation.subscribe(data => {
      this.invitation = data;
    });
  }

}
