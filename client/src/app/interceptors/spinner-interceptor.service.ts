import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError, delay, finalize, skip } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {

  constructor(private spinner: NgxSpinnerService, private toastr: ToastrService) {}

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!req.body) {
      this.spinner.show();
    }
    let shouldHide = true

    return next.handle(req).pipe(
      catchError((error) => {
        shouldHide = false;
        let errorMsg = '';
        if (error.error instanceof ErrorEvent) {
          errorMsg = `Error: ${error.error.message}`;
        }
        else {
          errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        }
        this.toastr.error(errorMsg, 'Error',{positionClass: 'toast-bottom-center', disableTimeOut: true})
        return of(error);
      }),
      delay(2500),
      finalize(() => {if(shouldHide) {console.log(shouldHide); this.spinner.hide()}})
    );
  }
}
