export interface Person {
  name: string;
  isChild: boolean;
  menu: 'fish' | 'meat' | 'vegie';
  going?: boolean;
  isNameValid?: boolean;
}

export interface Invitation {
  email: string;
  phone: string;
  numberOfGuests: number;
  invited?: Person[];
  additionalGuests: Person[];
  seen?: boolean;
  answered: boolean;
  kids?: number;
  plusOnes?: number;
  sameName?: string;
  number?: number;
  attend: number;
  singlePersonGoing?: string;
}
