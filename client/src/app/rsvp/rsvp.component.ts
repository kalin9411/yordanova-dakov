import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { Invitation, Person } from '../models/Models';

@Component({
  selector: 'app-rsvp',
  templateUrl: './rsvp.component.html',
  styleUrls: ['./rsvp.component.scss']
})
export class RsvpComponent implements OnInit {

  showForm = false;
  e = '';
  invitation: Invitation;
  backupInvitation: string;
  isUpdated = false;
  flexRadioDefault = '';
  isLoading: boolean;

  get saveDisabled(): boolean {
    const isEmailValid = this.isEmailValid();
    const isPhoneValid = this.isPhoneValid();
    const attendance = this.invitation.attend == null;
    const invitationIsSame = JSON.stringify(this.invitation) === this.backupInvitation;
    const invitedHaveSelectedMenus = this.invitation.invited.reduce((r, x) => {
      if (x.going && x.menu == null) {
        r = false;
      }
      return r;
    }, true);
    const hasPlusOnes = this.invitation.additionalGuests.length;
    const plusOnesAreValid = this.invitation.additionalGuests.reduce((r, x) => {
      if (!x.isNameValid || x.menu == null) {
        r = false;
      }
      return r;
    }, true);

    return !isEmailValid ||
      !isPhoneValid ||
      attendance ||
      invitationIsSame ||
      !invitedHaveSelectedMenus ||
      (hasPlusOnes && !plusOnesAreValid);
  }

  constructor(private backendService: BackendService) { }

  addOne(): void {
    this.invitation.additionalGuests.push({ isAttending: null, isChild: null, meal: null } as any);
  }

  removeOne(person): void {
    const index = this.invitation.additionalGuests.findIndex(f => f === person);
    this.invitation.additionalGuests.splice(index, 1);
  }

  isEmailValid(): boolean | string {
    if (!this.invitation.email) {
      return '';
    }
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.invitation.email).toLowerCase());
  }

  isPhoneValid(): boolean | string {
    if (!this.invitation.phone) {
      return '';
    }
    const re = /^[+]{1}[\s\0-9]{12}/g;
    return this.invitation.phone.replace(/ /g, '').replace(re, '') === '';
  }

  ngOnInit(): void {
    this.backendService.invitation.subscribe(data => {
      this.invitation = data;
      this.backupInvitation = JSON.stringify(data);
      this.e = data.invited.length > 1 ? 'e' : '';
      this.showForm = data.answered;
    });
  }

  updateInvitation(): void {
    this.backendService.updateInvitation(localStorage.getItem('id'), {
      number: this.invitation.number,
      email: this.invitation.email,
      phone: this.invitation.phone,
      numberOfGuests: this.invitation.attend + this.invitation.additionalGuests?.length,
      invited: this.invitation.invited,
      additionalGuests: this.invitation.additionalGuests || [],
      answered: true,
      attend: this.invitation.attend
    }).subscribe(data => {
      this.isUpdated = true;
    });
  }

  setPersonGoing(person): void {
    this.invitation.invited.forEach(i => i.going = i.name === person);
  }

  setGoingPeople(goingNumber: number): void {
    this.invitation.attend = goingNumber;
    const numberOfInvited = this.invitation.invited.length;
    this.invitation.singlePersonGoing = null;

    if (goingNumber === 0) {
      this.invitation.invited.forEach(i => i.going = false);
    } else if (goingNumber === numberOfInvited) {
      this.invitation.invited.forEach(i => i.going = true);
    } else {
      this.invitation.invited.forEach(i => i.going = false);
    }
  }

}
