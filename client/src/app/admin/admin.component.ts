import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  log
  seen
  questions

  constructor(private backendService: BackendService) { }

  ngOnInit(): void {
    this.backendService.getQuestions().subscribe(data => {
      this.questions = data
    });
    this.backendService.getLog().subscribe(data => {
      this.log = data;
      console.log(data)
    })
    this.backendService.getData().subscribe((data: []) => {
      this.seen = data.reduce((previous: {seen}[], current: {seen}, index: number) => {

        if(current.seen) {
          previous.push(current)
        }
        return previous
      }, []);
    })
  }

  stringify(data) {
    return JSON.stringify(data)
  }

}
