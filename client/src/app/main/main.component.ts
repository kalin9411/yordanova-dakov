import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {
  name: string;
  title = 'yordanova-dakov';

  constructor(
    private route: ActivatedRoute,
    private backendService: BackendService
  ) {}

  getInvitation() {
    this.route.queryParams.subscribe(data => {
      this.backendService.getInvitation(data?.invitation);
    })
  }

  ngAfterViewInit() {
    this.getInvitation()
  }

  ngOnInit() {

  }

}
