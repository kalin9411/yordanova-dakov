import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Person } from '../models/Models';

@Component({
  selector: 'app-plusone',
  templateUrl: './plusone.component.html',
  styleUrls: ['./plusone.component.scss']
})
export class PlusoneComponent implements OnInit {

  @Input()
  person: Person;

  @Output()
  minusOne = new EventEmitter();

  constructor() { }


  ngOnInit(): void {
  }

  isNameValid() {
    if (!this.person.name) return '';
    const re = /^[a-zA-Zа\u0400-\u04FF]+(?:\s+[a-zA-Z\u0400-\u04FF]+)*$/g;
    const isValid = re.test(String(this.person.name).toLowerCase()) && this.person.name.split(' ').length < 3;
    this.person.isNameValid = isValid;
    return isValid;
  }

  removeOne() {
    this.minusOne.emit(this.person)
  }
}
