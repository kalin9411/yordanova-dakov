import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlusoneComponent } from './plusone.component';

describe('PlusoneComponent', () => {
  let component: PlusoneComponent;
  let fixture: ComponentFixture<PlusoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlusoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlusoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
